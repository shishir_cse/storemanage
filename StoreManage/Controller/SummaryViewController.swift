//
//  SummaryViewController.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 21/2/22.
//

import UIKit
import ProgressHUD

class SummaryViewController: UIViewController {
    @IBOutlet weak var addresstextView: UITextView!
    @IBOutlet weak var confirmBtnOutlet: UIButton!
    @IBOutlet weak var totalCostLbl: UILabel!
    var selectedIndex = [IndexPath]()
    var selectedViewModel = ViewModel()
    var selectedProducts: [ProductsInfo]?
    
    var totalCost = CGFloat(0)
    var viewMode = ViewModel()
    var postModel = PostModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmBtnOutlet.layer.cornerRadius = 15
        getTotalCost()
    }
    
    func getTotalCost(){
        for indx in selectedIndex{
            totalCost+=viewMode.products![indx.row].price
            selectedProducts?.append(viewMode.products![indx.row])
        }
        
        totalCostLbl.text = "Your total cost " + "\(totalCost)"
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmOrderAction(_ sender: Any) {
        
        
        ProgressHUD.animationType = .circleStrokeSpin
        ProgressHUD.show("Loading")
        
        
        postModel.products = selectedProducts
        postModel.delivery_address = addresstextView.text
        
        do {
            let jsonData = try JSONEncoder().encode(postModel)
            print(jsonData)
            
            Networking.share.httpPost(jsonData: jsonData) { flag in
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                    ProgressHUD.dismiss()
                }
                
            }
        }catch {
            print(error.localizedDescription)
        }
    }
    
}
