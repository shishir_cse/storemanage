//
//  ViewController.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var orderBtnOutlet: UIButton!
    @IBOutlet weak var storeCollectionView: UICollectionView!
    
    var cellSpace = CGFloat(3)
    var viewModel: ViewModel!
    var selectedIndex = [IndexPath]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Networking.share.delegate = self
        initNIB()
        Networking.share.getAllInfoFromApi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
           
        }else{
            print("Internet Connection not Available!")
            noInternate()
        }
    }
    
    func initNIB(){
        
        storeCollectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemCollectionViewCell")
        
        storeCollectionView.register(UINib(nibName: "TopCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TopCollectionReusableView")
        
        storeCollectionView.register(UINib(nibName: "TopCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "TopCollectionReusableView")
        
        storeCollectionView.delegate = self
        storeCollectionView.dataSource = self
        orderBtnOutlet.layer.cornerRadius = 10
        orderBtnOutlet.isHidden = true
        
        navigationController?.navigationBar.isHidden = true
        
    }
    func noInternate(){
        
        OperationQueue.main.addOperation() {
            
            let alert = UIAlertController(title: "No Internate", message: "Internate required for data fetching", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: {(ACTION) in alert.dismiss(animated: true, completion: nil)
                
            }))
            self.present(alert,animated: true, completion: nil)
        }
    }
    
    
    @IBAction func viewSummaryAction(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
        
        vc.viewMode = viewModel
        vc.selectedIndex=selectedIndex
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
//MARK: - Data Source Delegate

extension ViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if viewModel != nil{
            return viewModel.products!.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = storeCollectionView.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        
        if let viewModel = viewModel{
            
            Networking.share.downloadImage(imgURL: URL(string: viewModel.products![indexPath.row].imageUrl)!, completion: { image in
                cell.thumbImageOutlet.image = image
            })
            
            cell.itemNameLbl.text = viewModel.products![indexPath.row].name
            cell.priceLbl.text = "\(viewModel.products![indexPath.row].price)"
        }
        
        if  selectedIndex.contains(indexPath){
            cell.containerView.backgroundColor = .red
        }else{
            cell.containerView.backgroundColor = .clear
        }
        
        
        return cell
    }
    
    
}

extension ViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if  selectedIndex.contains(indexPath){
            
            let indx = selectedIndex.firstIndex(of: indexPath)
            selectedIndex.remove(at: indx!)
            
        }else{
            selectedIndex.append(indexPath)
        }
        
        storeCollectionView.reloadItems(at: [indexPath])
        
        if selectedIndex.count>0{
            orderBtnOutlet.isHidden = false
        }else{
            orderBtnOutlet.isHidden = true
        }
    }
    
}


//MARK: - CollectionView Flow Layout -

extension ViewController : UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            let headerView = storeCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TopCollectionReusableView", for: indexPath) as! TopCollectionReusableView
            
            if let viewModel = viewModel{
                
                headerView.nameeLblOutlet.text = viewModel.storeInfo!.name
                headerView.ratingLblOutlet.text = "\(viewModel.storeInfo!.rating!)"
                headerView.openingTimeLbl.text = viewModel.storeInfo?.openingTime
                headerView.closingTimeLbl.text = viewModel.storeInfo?.closingTime
            }
            
            return headerView
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TopCollectionReusableView", for: indexPath) as! TopCollectionReusableView
            
            footerView.backgroundColor = .clear
            footerView.containerView.isHidden = true
            return footerView
            
        default:
            fatalError("Unexpected element kind")
            //assert(false, "Unexpected element kind")
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width  = Int(storeCollectionView.frame.size.width)-Int(cellSpace)
        
        return CGSize(width: width, height: width)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 260)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpace
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpace
    }
}

//MARK: - Networking Delegate Method -

extension ViewController: NetworkingDelegate{
    func fetchTheStoreAllInfo(viewModel: ViewModel?) {
        self.viewModel = viewModel
        storeCollectionView.reloadData()
    }
    
    
}
