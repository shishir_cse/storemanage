//
//  ItemCollectionViewCell.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var thumbImageOutlet: UIImageView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.layer.cornerRadius = 15
        thumbImageOutlet.layer.cornerRadius = 15
    }

}
