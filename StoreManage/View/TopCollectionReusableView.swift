//
//  TopCollectionReusableView.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import UIKit

class TopCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameeLblOutlet: UILabel!
    
    @IBOutlet weak var ratingLblOutlet: UILabel!
    
    @IBOutlet weak var openingTimeLbl: UILabel!
    
    @IBOutlet weak var closingTimeLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // containerView.layer.cornerRadius = 20
        // Initialization code
    }
    
}
