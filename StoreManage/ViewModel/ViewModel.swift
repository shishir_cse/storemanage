//
//  ViewModel.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import Foundation
import UIKit

struct ViewModel{
    
    var storeInfo: StoreInfo?
    var products: [ProductsInfo]?
}
