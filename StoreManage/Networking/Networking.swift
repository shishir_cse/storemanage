//
//  Networking.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import Foundation
import UIKit

class Networking{
    
    static var share = Networking()
    static var cache = NSCache<AnyObject, UIImage>()
    var dispatch = DispatchGroup()
    var viewModel = ViewModel()
    var delegate: NetworkingDelegate!
    private init(){}
    
    func getAllInfoFromApi(){
        
        dispatch.enter()
        getStoreInfoFromAPI { [self] storeInfo, falg in
            viewModel.storeInfo = storeInfo
            dispatch.leave()
        }
        
        dispatch.enter()
        getproductsInfoFromAPI { [self] allProducts, flag in
            viewModel.products = allProducts
            dispatch.leave()
        }
        
        dispatch.notify(queue: .main) { [self] in
            
            delegate.fetchTheStoreAllInfo(viewModel: viewModel)
            print("All done!!")
        }
        
    }
    
    
    func getStoreInfoFromAPI(completion: @escaping ( StoreInfo, Bool?) -> ()){
        
        let urlStr = "https://c8d92d0a-6233-4ef7-a229-5a91deb91ea1.mock.pstmn.io/storeInfo"
        let session = URLSession.shared
        guard let url = URL(string: urlStr) else {
            return
        }
        session.dataTask(with: url) { (data, response, err) in
            guard let data = data else{return}
            
            do{
                let json  = try JSONDecoder().decode(StoreInfo.self, from: data)
                
                print(json)
                
                completion(json, true)
                
            }
            catch let jeerr
            {
                print(jeerr.localizedDescription)
            }
        }.resume()
        
    }
    
    func getproductsInfoFromAPI(completion: @escaping ( [ProductsInfo], Bool?) -> ()){
        
        let urlStr = "https://c8d92d0a-6233-4ef7-a229-5a91deb91ea1.mock.pstmn.io/products"
        let session = URLSession.shared
        guard let url = URL(string: urlStr) else {
            return
        }
        session.dataTask(with: url) { (data, response, err) in
            guard let data = data else{return}
            
            do{
                let json  = try JSONDecoder().decode([ProductsInfo].self, from: data)
                
                print(json)
                
                completion(json, true)
                
            }
            catch let jeerr
            {
                print(jeerr.localizedDescription)
            }
        }.resume()
        
    }
    
    
    func downloadImage(imgURL: URL, completion: @escaping (UIImage?)->()){
        
        if let cachedImage = Networking.cache.object(forKey: imgURL as AnyObject) {
           // print("You get image from cache")
            completion(cachedImage)
            
        }else{
            URLSession.shared.dataTask(with: imgURL) { (data, respnse, error) in
                if let error = error {
                    print("Error: \(error)")
                    
                }else if let data = data {
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        
                        if let img = image{
                            Networking.cache.setObject(img, forKey: imgURL as AnyObject)
                          //  print("You get image from \(imgURL)")
                            completion(img)
                        }else{
                            completion(nil)
                        }
                    }
                }
            }.resume()
        }
    }
    
    
    func httpPost(jsonData: Data, complition: @escaping (Bool)->()) {
        
        let url = URL(string: "https://c8d92d0a-6233-4ef7-a229-5a91deb91ea1.mock.pstmn.io/order")!
        if !jsonData.isEmpty {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = jsonData
            
            URLSession.shared.getAllTasks { (openTasks: [URLSessionTask]) in
                NSLog("open tasks: \(openTasks)")
                
            }
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (responseData: Data?, response: URLResponse?, error: Error?) in
                print(response?.url?.dataRepresentation as Any)
                complition(true)
            })
            task.resume()
        }
    }
    deinit {
        
        print("Deinit NetworkManager")
    }
    
}

protocol NetworkingDelegate: AnyObject{
    func fetchTheStoreAllInfo(viewModel: ViewModel?)
}
