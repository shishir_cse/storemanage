//
//  StoreInfo.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import Foundation
import UIKit

struct StoreInfo: Codable{
    
    var name: String?
    var rating: CGFloat?
    var openingTime: String?
    var closingTime: String?
}
/*
{
 "name": "The Coffee Shop",
 "rating": 4.5,
 "openingTime": "15:01:01.772Z",
 "closingTime": "19:45:51.365Z"
}
*/
