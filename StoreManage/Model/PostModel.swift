//
//  PostModel.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 21/2/22.
//

import Foundation

struct PostModel: Codable{
    
    var products: [ProductsInfo]?
    var delivery_address: String?
    
}
