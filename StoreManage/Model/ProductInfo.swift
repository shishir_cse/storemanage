//
//  ProductInfo.swift
//  StoreManage
//
//  Created by Md Saddam Hossain on 19/2/22.
//

import Foundation
import UIKit


struct ProductsInfo: Codable{
    var name: String
    var price: CGFloat
    var imageUrl: String
}


/*
 [
  {
   "name": "Latte",
   "price": 50,
   "imageUrl": "https://www.nespresso.com/ncp/res/uploads/recipes/nespresso-recipes-Latte-Art-Tulip.jpg"
  },
  {
   "name": "Dark Tiramisu Mocha",
   "price": 75,
   "imageUrl": "https://www.nespresso.com/shared_res/mos/free_html/sg/b2b/b2ccoffeerecipes/listing-image/image/dark-tiramisu-mocha.jpg"
  }
 ]
 */
